let input, button, sel, checkbox, slider;
let img;
let c;
let customFont;
let inp;
let displayText = "ZWICKL";
let imageTypeN = 0;
let tintImage = false;

//Dithering
let black;
let s = 200;
var graphics;

var res = 2;

let typeImages = [];

let controlsPosition = [600, 20];
let controlsPositionSpacing = 40;

function preload() {
    for (var i = 0; i <= 7; i++) {
        typeImages[i] = loadImage(`img/${i}.png`);
    }
}

function setup() {
    pixelDensity(6.0);
    c = createCanvas(500, 500);
    graphics = createGraphics(width, height);
    customFont = loadFont("GTAmerica.otf");
    textFont(customFont);
    textSize(48);
    textAlign(LEFT);

    input = createFileInput(handleFile);
    input.position(controlsPosition[0], controlsPosition[1]);

    span = createSpan("Image Zoom");
    span.position(controlsPosition[0], controlsPosition[1] + controlsPositionSpacing-10);
    slider = createSlider(0, 255, 100);
    slider.position(controlsPosition[0], controlsPosition[1] + controlsPositionSpacing);
    slider.style('width', '160px');

    inp = createElement('textarea');
    inp.position(controlsPosition[0], controlsPosition[1] + controlsPositionSpacing*2);

    inp.value(displayText)
    inp.input(inputChange);

    sel = createSelect();
    sel.position(controlsPosition[0], controlsPosition[1] + controlsPositionSpacing * 4);
    sel.option('Regulärprogramm');
    sel.option('Freiheit');
    sel.option('Werkschau');
    sel.option('ZETT');
    sel.option('ZWICKERL');
    sel.option('ZETT Selection');
    sel.option('ZETT Winner');
    sel.option('Team');
    sel.option('(Leer)');
    sel.changed(selectType);

    checkbox = createCheckbox('Bild gelb färben', false);
    checkbox.position(controlsPosition[0], controlsPosition[1] + controlsPositionSpacing * 5);
    checkbox.changed(myCheckedEvent);

    button = createButton('save');
    button.position(controlsPosition[0], controlsPosition[1] + controlsPositionSpacing * 6);
    button.mousePressed(saveImage);
}

function draw() {
    blendMode(BLEND);
    background("#e3e541");
    if (img) {
        if (tintImage) {
            let ditherType = 'atkinson';
            graphics.background(255);
            graphics.image(img, 0-slider.value()*2, 0-slider.value()*2, width+slider.value()*4, height+slider.value()*4, 0, 0, img.width, img.height, COVER);

            let dithered = ditherImage(graphics, ditherType, 1);
            blendMode(MULTIPLY)
            image(dithered, 0, 0);
        } else {
            image(img, 0-slider.value()*2, 0-slider.value()*2, width+slider.value()*4, height+slider.value()*4, 0, 0, img.width, img.height, COVER);
            //filter(GRAY);
        }

    }
    blendMode(BLEND);
    fill("#e3e541")
    textLeading(40);
    text(displayText, 10, 48)

    let numberofimage = 3;
    if (imageTypeN == "ZETT") numberofimage = 0;
    if (imageTypeN == "Regulärprogramm") numberofimage = 3;
    if (imageTypeN == "ZWICKERL") numberofimage = 1;
    if (imageTypeN == "Freiheit") numberofimage = 2;
    if (imageTypeN == "Werkschau") numberofimage = 4;
    if (imageTypeN == "(Leer)") numberofimage = -1;
    if (imageTypeN == "ZETT Selection") numberofimage = 6;
    if (imageTypeN == "ZETT Winner") numberofimage = 5;
    if (imageTypeN == "Team") numberofimage = 7;


    if (numberofimage != -1) {
        var scale = 1;
        var scaleY = 1;
        if(numberofimage>=5) {scale = 4;scaleY = 1.5}
        if(numberofimage>=7) {scale = 2;scaleY = 1}
        //tint("#e3e541");
       if(tintImage && numberofimage>=5) tint("#e3e541");
       else noTint();
        image(typeImages[numberofimage], width-20-50*scale, 20, 50*scale, 50*scaleY,0, 0, typeImages[numberofimage].width, typeImages[numberofimage].height, CONTAIN);
    }

}

function handleFile(file) {
    print(file);
    if (file.type === 'image') {
        img = createImg(file.data, '');
        img.hide();
    } else {
        img = null;
    }
}

function inputChange() {
    displayText = inp.value()
}

function selectType() {
    imageTypeN = sel.value()
}


function saveImage() {
    saveCanvas(c, 'image.jpg');
}

function myCheckedEvent(){
    if (checkbox.checked()) {
        tintImage = true;
      } else {
        tintImage = false;
      }
}